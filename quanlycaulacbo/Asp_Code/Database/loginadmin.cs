﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;


namespace quanlycaulacbo
{
    public class loginadmin
    {
        public static DataTable admin(string username,
                                         string password)
        {
            OleDbCommand cmd = new OleDbCommand("select * from ADMIN where username = ? and password = ?");
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("username", username);
            cmd.Parameters.AddWithValue("password", password);
            return SQLDatabase.GetData(cmd);
        }
    }
}