﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;

namespace quanlycaulacbo
{

    public class quanlymember
    {
        public static void Member_Delete(string iD)
        {
            OleDbCommand cmd = new OleDbCommand("delete from Member where iD = ?");
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("iD", iD);
            SQLDatabase.ExecuteNoneQuery(cmd);
        }

        public static void Member_Insert(
                                            
                                            string masv,
                                            string hinhanh,
                                            string tenmb,
                                            string namsinh,
                                            string thuockhoa)
        {
            OleDbCommand cmd = new OleDbCommand("insert into Member(masv, hinhanh, tenmb, namsinh, thuockhoa) VALUES (?,?,?,?,?)");
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("masv", masv);
            cmd.Parameters.AddWithValue("hinhanh", hinhanh);
            cmd.Parameters.AddWithValue("tenmb", tenmb);
            cmd.Parameters.AddWithValue("namsinh", namsinh);
            cmd.Parameters.AddWithValue("thuockhoa", thuockhoa);
            SQLDatabase.ExecuteNoneQuery(cmd);
        }

        public static void Member_Update(
                                            string masv,
                                            string hinhanh,
                                            string tenmb,
                                            string namsinh,
                                            string thuockhoa,
            string id
                                            )
        {
            OleDbCommand cmd = new OleDbCommand("update Member set masv = ?, hinhanh =?, tenmb = ?, namsinh = ?, thuockhoa = ? where iD =?");
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("maMayTinh", masv);
            cmd.Parameters.AddWithValue("hinhAnh", hinhanh);
            cmd.Parameters.AddWithValue("tenMayTinh", tenmb);
            cmd.Parameters.AddWithValue("loaiMayTinh", namsinh);
            cmd.Parameters.AddWithValue("donViSanXuat", thuockhoa);
            cmd.Parameters.AddWithValue("iD", id);
            SQLDatabase.ExecuteNoneQuery(cmd);

        }
        
        public static DataTable ThongTin_Member()
        {
            OleDbCommand cmd = new OleDbCommand("select * from Member");
            cmd.CommandType = System.Data.CommandType.Text;
            return SQLDatabase.GetData(cmd);

        }

        public static DataTable ThongTin_Member_hoten(string tenmaytinh)
        {
            OleDbCommand cmd = new OleDbCommand("select * from Member where tenmb = ?");
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("tenmb", tenmaytinh);
            return SQLDatabase.GetData(cmd);

        }
        public static DataTable ThongTin_Member_iD(string iD)
        {
            OleDbCommand cmd = new OleDbCommand("select * from Member where iD = ?");
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("iD", iD);
            return SQLDatabase.GetData(cmd);

        }
    }
}
