﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;

namespace quanlycaulacbo
{
    public class qlev
    {
        public static void Insert_Event(
                                                    string tenev,
                                                    string ngaydienra,
                                                    string ghichu)
        {
            OleDbCommand cmd = new OleDbCommand("insert into EVENT(tenev, ngaydienra, ghichu) VALUES (?, ?,?)");
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("tenev", tenev);
            cmd.Parameters.AddWithValue("ngaydienra", ngaydienra);
            cmd.Parameters.AddWithValue("ghichu", ghichu);
            SQLDatabase.ExecuteNoneQuery(cmd);
        }
      
        public static void Delete_Event(int idev)
        {
            OleDbCommand cmd = new OleDbCommand("delete from EVENT where idev = ?");
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("idev", idev);
            SQLDatabase.ExecuteNoneQuery(cmd);
        }
        public static void Update_Event(
                                                    string tenev,
                                                    string ngaydienra,
                                                    string ghichu,
                                                    string idev
            )
        {
            OleDbCommand cmd = new OleDbCommand("update EVENT set tenev = ?, ngaydienra = ?, ghichu = ? where idev = ?");
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("tenev", tenev);
            cmd.Parameters.AddWithValue("ngaydienra", ngaydienra);
            cmd.Parameters.AddWithValue("ghichu", ghichu);
            cmd.Parameters.AddWithValue("idev", idev);
            SQLDatabase.ExecuteNoneQuery(cmd);
        }
        public static DataTable ThongTin_Event()
        {
            OleDbCommand cmd = new OleDbCommand("select * from EVENT");
            cmd.CommandType = CommandType.Text;
            return SQLDatabase.GetData(cmd);
        }
        public static DataTable ThongTin_Event_id(string id)
        {
            OleDbCommand cmd = new OleDbCommand("select * from EVENT where idev = ?");
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("idev", id);
            return SQLDatabase.GetData(cmd);

        }
        public static DataTable ThongTin_Event_iD(int id)
        {
            OleDbCommand cmd = new OleDbCommand("select * from EVENT where idev = ?");
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("idev", id);
            return SQLDatabase.GetData(cmd);

        }
    }
}