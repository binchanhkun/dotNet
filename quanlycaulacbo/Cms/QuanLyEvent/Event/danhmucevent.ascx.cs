﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace quanlyphongthuchanh.Cms.QuanLyEvent.Event
{
    public partial class danhmucevent : System.Web.UI.UserControl
    {
        private string iD = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                thongtinevent();
                // btnsearch_Click(sender, e);

            }
        }

        private void thongtinevent()
        {
            DataTable dt = new DataTable();
            dt = quanlycaulacbo.qlev.ThongTin_Event();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                HienThiEvent.Text += @"
             <tr id='madong_" + dt.Rows[i]["idev"] + @"'>
            <td class='cotidev'>" + dt.Rows[i]["idev"] + @"</td>
            <td class='cottenev'>" + dt.Rows[i]["tenev"] + @"</td>
            <td class='cotngaydienraev'>" + dt.Rows[i]["ngaydienra"] + @"</td>
            <td class='cotghichu'>" + dt.Rows[i]["ghichu"] + @"</td>
            <td class='cotcongcu'>
                <a class='logoadd' title='Thêm' href='AdminDhv.aspx?modul=qlth&thaotac=themmoiev'><i class='far fa-plus-square'></i></a>
                <a class='logorepaid' title='Sửa' href='AdminDhv.aspx?modul=qlth&thaotac=chinhsuaev&idev=" + dt.Rows[i]["idev"] + @"'><i class='fas fa-tools'></i></a>
                <a class='logodelete' title='Xóa' href='javascript:XoaEvent(" + dt.Rows[i]["idev"] + @")'><i class='fas fa-trash-alt'></i></a>
            </td>
        </tr>
";
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            int str = int.Parse(txtsearch.Text);
            DataTable dt2 = new DataTable();
            dt2 = quanlycaulacbo.qlev.ThongTin_Event_iD(str);
            HienThiEvent.Text = "";
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                HienThiEvent.Text += @"
              <tr id='madong_" + dt2.Rows[i]["idev"] + @"'>
            <td class='cotidev'>" + dt2.Rows[i]["idev"] + @"</td>
            <td class='cottenev'>" + dt2.Rows[i]["tenev"] + @"</td>
            <td class='cotngaydienraev'>" + dt2.Rows[i]["ngaydienra"] + @"</td>
            <td class='cotghichu'>" + dt2.Rows[i]["ghichu"] + @"</td>
            <td class='cotcongcu'>
                <a class='logoadd' title='Thêm' href='AdminDhv.aspx?modul=qlth&thaotac=themmoiev'><i class='far fa-plus-square'></i></a>
                <a class='logorepaid' title='Sửa' href='AdminDhv.aspx?modul=qlth&thaotac=chinhsuaev&idev=" + dt2.Rows[i]["idev"] + @"'><i class='fas fa-tools'></i></a>
                <a class='logodelete' title='Xóa' href='javascript:XoaEvent(" + dt2.Rows[i]["idev"] + @")'><i class='fas fa-trash-alt'></i></a>
            </td>
        </tr>
";
            }

        }
    }
}