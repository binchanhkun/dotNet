﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace quanlyphongthuchanh.Cms.QuanLyEvent.Event
{
    public partial class themmoievent : System.Web.UI.UserControl
    {
        private string thaotac = "";
        private string id = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] != null && Session["login"].ToString() == "1")
            {

            }
            else
            {
                Response.Redirect("/login.aspx");
            }
            if (Request.QueryString["thaotac"] != null)
                thaotac = Request.QueryString["thaotac"];
            if (Request.QueryString["idev"] != null)
                id = Request.QueryString["idev"];

            if (!IsPostBack)
            {

                hienthithongtin(id);

            }
        }

        private void hienthithongtin(string id)
        {
            if (thaotac == "chinhsuaev")
            {
                cbthemmoi.Visible = false;
                btnthemmoi.Text = "Chỉnh Sửa";
                DataTable dt = new DataTable();
                dt = quanlycaulacbo.qlev.ThongTin_Event_id(id);
                if (dt.Rows.Count > 0)
                {
                    idev.Text = dt.Rows[0]["idev"].ToString();
                    tenev.Text = dt.Rows[0]["tenev"].ToString();
                    ngaydienra.Text = dt.Rows[0]["ngaydienra"].ToString();
                    ghichu.Text = dt.Rows[0]["ghichu"].ToString();   
                }
                else
                {
                    btnthemmoi.Text = "Thêm mới";
                    cbthemmoi.Visible = true;

                }
            }
        }



        protected void btnthemmoi_Click(object sender, EventArgs e)
        {
            if (thaotac == "themmoiev")
            {
                
                quanlycaulacbo.qlev.Insert_Event(tenev.Text, ngaydienra.Text, ghichu.Text);
                Response.Redirect("AdminDhv.aspx?modul=qlth&modulphu=danhmucevent");
            }
            else
            {
                
                quanlycaulacbo.qlev.Update_Event(tenev.Text, ngaydienra.Text, ghichu.Text, id);

                Response.Redirect("AdminDhv.aspx?modul=qlth&modulphu=danhmucevent");
            }
        }
        private void resetControl()
        {
            tenev.Text = "";
            ngaydienra.Text = "";
            ghichu.Text = "";
        }
        protected void btnhuy_Click(object sender, EventArgs e)
        {
            resetControl();
        }
    }
}