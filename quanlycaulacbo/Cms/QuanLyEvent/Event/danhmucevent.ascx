﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="danhmucevent.ascx.cs" Inherits="quanlyphongthuchanh.Cms.QuanLyEvent.Event.danhmucevent" %>
<div class="head_hienthi">
    <p style="background-color:#1d8cf8;height: 30px;margin-top: 0px; text-align:center; font-weight:600">Hiển thị thông tin máy tính</p>
</div>
<div class="khoangcach">
    <table class="hienthi">
        <tr>
                <th>IDEV</th>
                <th>Tên EV</th>
                <th>Ngày diễn ra</th>
                <th>Ghi chú</th>
                <th>Thêm, sửa, xóa</th>
        </tr>
        <asp:Literal ID="HienThiEvent" runat="server"></asp:Literal>
    </table>
</div>

<script type="text/javascript">
    function XoaEvent(iD) {
        if (confirm("Bạn chắc chắn muốn xóa event này?")) {

            $.post("Cms/QuanlyClub/QuanLyDanhMucTV/Ajax/Danhmucevent.aspx",
                {
                    "thaotac": "XoaEvent",
                    "idev": iD
                },
                
                function (data, status) {
                        $("#madong_" + iD).slideUp();
                });
        }
    }
</script>
<asp:TextBox  ID="txtsearch" placeholder="Nhập tên id event cần tìm" runat="server" ></asp:TextBox>
<asp:Button ID="btnsearch" runat="server" Height="29px" OnClick="btnsearch_Click" Text="Tìm kiếm" Width="98px" />