﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace quanlyphongthuchanh.Cms.QuanLyThucHanh
{
    public partial class qlth : System.Web.UI.UserControl
    {
        private string modulphu ="";
        private string thaotac = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["modulphu"] != null)
                modulphu = Request.QueryString["modulphu"];
            switch (modulphu)
            {
                case "danhmucevent":
                    plqlthloadcontrol.Controls.Add(LoadControl("Event/danhmucevent.ascx"));
                    break;
            }
            if (Request.QueryString["thaotac"] != null)
                thaotac = Request.QueryString["thaotac"];
            switch (thaotac)
            {
                case "themmoiev":
                    plqlthloadcontrol.Controls.Add(LoadControl("Event/themmoievent.ascx"));
                    break;
                case "chinhsuaev":
                    plqlthloadcontrol.Controls.Add(LoadControl("Event/themmoievent.ascx"));
                    break;
            }

        }
    }
}