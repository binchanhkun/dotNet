﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace quanlyphongthuchanh.Cms
{
    public partial class adminloadcontrol : System.Web.UI.UserControl
    {
        private string modul="";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["modul"] != null)
                modul = Request.QueryString["modul"];
            switch (modul)
            {
                case "qlmt":
                    pladminloadcotrol.Controls.Add(LoadControl("QuanLyClub/qlmt.ascx"));
                    break;

                case "qlth":
                    pladminloadcotrol.Controls.Add(LoadControl("QuanLyEvent/qlth.ascx"));
                    break;

                case "qltk":
                    pladminloadcotrol.Controls.Add(LoadControl("TaiKhoanAdmin/adminaccount.ascx"));
                    break;
            }
        }
    }
}