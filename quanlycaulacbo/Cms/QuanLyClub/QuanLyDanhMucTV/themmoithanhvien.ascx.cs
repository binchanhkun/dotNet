﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace quanlycaulacbo.Cms.QuanLyClub.QuanLyDanhMucTV
{
    public partial class themmoithanhvien : System.Web.UI.UserControl
    {
        private string thaotac = "";
        private string iD = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] != null && Session["login"].ToString() == "1")
            {

            }
            else
            {
                Response.Redirect("/login.aspx");
            }
            if (Request.QueryString["thaotac"] != null)
                thaotac = Request.QueryString["thaotac"];
            if (Request.QueryString["iD"] != null)
                iD = Request.QueryString["iD"];

            if (!IsPostBack)
            {

                hienthithongtin(iD);

            }
        }

        private void hienthithongtin(string iD)
        {
            if (thaotac == "chinhsua")
            {
                cbthemmoi.Visible = false;
                btnthemmoi.Text = "Chỉnh Sửa";
                DataTable dt = new DataTable();
                dt = quanlycaulacbo.quanlymember.ThongTin_Member_iD(iD);
                if (dt.Rows.Count > 0)
                {
                    namsinh.Text = dt.Rows[0]["namsinh"].ToString();
                    tenmb.Text = dt.Rows[0]["tenmb"].ToString();
                    masv.Text = dt.Rows[0]["masv"].ToString();
                    thuockhoa.Text = dt.Rows[0]["thuockhoa"].ToString();
                    anhchinhsua.Text = "<img class='anhchinhsua' src='img/Member/" + dt.Rows[0]["hinhanh"] + @"'/>";
                    tenanhcu.Value = dt.Rows[0]["hinhanh"].ToString();
                }
                else
                {
                    btnthemmoi.Text = "Thêm mới";
                    cbthemmoi.Visible = true;

                }
            }
        }



        protected void btnthemmoi_Click(object sender, EventArgs e)
        {
      if (thaotac == "themmoimt")
       {
                if (hinhanh.FileContent.Length > 0)
                {
                    if (hinhanh.FileName.EndsWith(".jpg") || hinhanh.FileName.EndsWith(".jpeg") || hinhanh.FileName.EndsWith(".png") || hinhanh.FileName.EndsWith(".gif"))
                    {
                        hinhanh.SaveAs(Server.MapPath("img/Member/") + hinhanh.FileName);
                    }
                }
                quanlycaulacbo.quanlymember.Member_Insert(masv.Text, hinhanh.FileName, tenmb.Text, namsinh.Text, thuockhoa.Text);
                    Response.Redirect("AdminDhv.aspx?modul=qlmt&modulphu=danhmucmt");
            }
            else
             {
             string tenanhdaidien = "";
             if (hinhanh.FileContent.Length > 0)
             {
                if (hinhanh.FileName.EndsWith(".jpg") || hinhanh.FileName.EndsWith(".jpeg") || hinhanh.FileName.EndsWith(".png") || hinhanh.FileName.EndsWith(".gif"))
               {
                 hinhanh.SaveAs(Server.MapPath("img/Member/") + hinhanh.FileName);
                 tenanhdaidien = hinhanh.FileName;
         }
       }
        if(tenanhdaidien== "")
        {
          tenanhdaidien = tenanhcu.Value;
        }    
          quanlycaulacbo.quanlymember.Member_Update(masv.Text, hinhanh.FileName, tenmb.Text, namsinh.Text, thuockhoa.Text, iD);

           Response.Redirect("AdminDhv.aspx?modul=qlmt&modulphu=danhmucmt");
        }
         }
        private void resetControl()
        {
            tenmb.Text = "";
            masv.Text = "";
            namsinh.Text = "";
            thuockhoa.Text = "";
        }
        protected void btnhuy_Click(object sender, EventArgs e)
        {
            resetControl();
        }
    }
}