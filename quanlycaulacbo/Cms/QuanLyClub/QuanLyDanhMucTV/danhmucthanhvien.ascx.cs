﻿using System;
using System.Data;

namespace quanlycaulacbo.Cms.QuanLyClub.QuanLyDanhMucTV
{
    public partial class danhmucthanhvien : System.Web.UI.UserControl

    {
        private string iD = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                thongtinmaytinh();
                // btnsearch_Click(sender, e);
               
            }
        }

        private void thongtinmaytinh()
        {
            DataTable dt = new DataTable();
            dt = quanlycaulacbo.quanlymember.ThongTin_Member();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                HienThiClub.Text += @"
             <tr id='madong_" + dt.Rows[i]["iD"] + @"'>
            <td class='cotid'>" + dt.Rows[i]["iD"] + @"</td>
            <td class='cotmamaytinh'>" + dt.Rows[i]["masv"] + @"</td>
            <td class='cothinhanh'><img class='anh' src='img/Member/" + dt.Rows[i]["hinhanh"] + @"'/>
                                    <img class='anhhover' src='img/Member/" + dt.Rows[i]["hinhanh"] + @"'/></td>
            <td class='cottenmt'>" + dt.Rows[i]["tenmb"] + @"</td>
            <td class='cotloaimt'>" + dt.Rows[i]["namsinh"] + @"</td>
            <td class='cotdonvisx'>" + dt.Rows[i]["thuockhoa"] + @"</td>
            <td class='cotcongcu'>
                <a class='logoadd' title='Thêm' href='AdminDhv.aspx?modul=qlmt&thaotac=themmoimt'><i class='far fa-plus-square'></i></a>
                <a class='logorepaid' title='Sửa' href='AdminDhv.aspx?modul=qlmt&thaotac=chinhsua&iD=" + dt.Rows[i]["iD"] + @"'><i class='fas fa-tools'></i></a>
                <a class='logodelete' title='Xóa' href='javascript:XoaThanhVien(" + dt.Rows[i]["iD"] + @")'><i class='fas fa-trash-alt'></i></a>
            </td>
        </tr>
";
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            string str = txtsearch.Text;
            DataTable dt2 = new DataTable();
            dt2 = quanlycaulacbo.quanlymember.ThongTin_Member_hoten(str);
            HienThiClub.Text = "";
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                HienThiClub.Text += @"
             <tr id='madong_" + dt2.Rows[i]["iD"] + @"'>
            <td class='cotid'>" + dt2.Rows[i]["iD"] + @"</td>
            <td class='cotmamaytinh'>" + dt2.Rows[i]["masv"] + @"</td>
            <td class='cothinhanh'><img class='anh' src='img/Member/" + dt2.Rows[i]["hinhanh"] + @"'/>
                                    <img class='anhhover' src='img/Member/" + dt2.Rows[i]["hinhanh"] + @"'/></td>
            <td class='cottenmt'>" + dt2.Rows[i]["tenmb"] + @"</td>
            <td class='cotloaimt'>" + dt2.Rows[i]["namsinh"] + @"</td>
            <td class='cotdonvisx'>" + dt2.Rows[i]["thuockhoa"] + @"</td>
            <td class='cotcongcu'>
                <a class='logoadd' title='Thêm' href='AdminDhv.aspx?modul=qlmt&thaotac=themmoimt'><i class='far fa-plus-square'></i></a>
                <a class='logorepaid' title='Sửa' href='AdminDhv.aspx?modul=qlmt&thaotac=chinhsua&iD=" + dt2.Rows[i]["iD"] + @"'><i class='fas fa-tools'></i></a>
                <a class='logodelete' title='Xóa' href='javascript:XoaThanhVien(" + dt2.Rows[i]["iD"] + @")'><i class='fas fa-trash-alt'></i></a>
            </td>
        </tr>
";
            }

        }
    }
}