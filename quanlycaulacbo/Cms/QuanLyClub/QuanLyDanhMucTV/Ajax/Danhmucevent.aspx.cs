﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace quanlyphongthuchanh.Cms.QuanLyClub.QuanLyDanhMucTV.Ajax
{
    public partial class Danhmucevent : System.Web.UI.Page
    {
        string thaotac = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["thaotac"] != null)
                thaotac = Request.Params["thaotac"];
            switch (thaotac)
            {
                case "XoaEvent":
                    XoaEvent();
                    break;
            }
        }
        private void XoaEvent()
        {
            string iD = "";
            if (Request.Params["idev"] != null)
                iD = Request.Params["idev"];
            quanlycaulacbo.quanlymember.Member_Delete(iD);

        }
    }
}