﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace quanlycaulacbo.Cms.QuanLyClub.QuanLyDanhMucTV.Ajax
{
    public partial class Danhmuctv : System.Web.UI.Page
    {
        string thaotac = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["thaotac"] != null)
                thaotac = Request.Params["thaotac"];
            switch(thaotac){
                case "XoaThanhVien":
                    XoaThanhVien();
                    break;
            }
        }
        private void XoaThanhVien()
        {
            string iD = "";
            if (Request.Params["iD"] != null)
                iD = Request.Params["iD"];
            quanlycaulacbo.quanlymember.Member_Delete(iD);

        }
    }
}