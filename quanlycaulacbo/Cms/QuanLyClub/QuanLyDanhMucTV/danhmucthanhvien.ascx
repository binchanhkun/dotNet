﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="danhmucthanhvien.ascx.cs" Inherits="quanlycaulacbo.Cms.QuanLyClub.QuanLyDanhMucTV.danhmucthanhvien" %>
<div class="head_hienthi">
    <p style="background-color:#1d8cf8;height: 30px;margin-top: 0px; text-align:center; font-weight:600">Hiển thị thông tin máy tính</p>
</div>
<div class="khoangcach">
    <div style="align-content:center;align-items:center; justify-content:center; display:flex; margin:10px 5px;">
    <asp:TextBox  ID="txtsearch" style="height:25px" placeholder="Nhập tên thành viên cần tìm" runat="server" ></asp:TextBox>
<asp:Button ID="btnsearch" runat="server" Height="29px" OnClick="btnsearch_Click" Text="Tìm kiếm" Width="98px" />
        </div>
    <table class="hienthi">
        <tr>
                <th>ID</th>
                <th>Mã sinh viên</th>
                <th>Hình ảnh</th>
                <th>Tên sinh viên</th>
                <th>Năm sinh</th>
                <th>Thuộc khoa</th>
                <th>Thêm, sửa, xóa</th>
        </tr>
        <asp:Literal ID="HienThiClub" runat="server"></asp:Literal>
    </table>
</div>

<script type="text/javascript">
    function XoaThanhVien(iD) {
        if (confirm("Bạn chắc chắn muốn xóa thành viên này?")) {

            $.post("Cms/QuanlyClub/QuanLyDanhMucTV/Ajax/Danhmuctv.aspx",
                {
                    "thaotac": "XoaThanhVien",
                    "iD": iD
                },
                
                function (data, status) {
                        $("#madong_" + iD).slideUp();
                });
        }
    }
</script>

