﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace quanlyphongthuchanh.Cms.QuanLyMayTinh
{
    public partial class qlmt : System.Web.UI.UserControl
    {
        private string modulphu="";
        private string thaotac= "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["modulphu"] != null)
            {
                modulphu = Request.QueryString["modulphu"];
                switch (modulphu)
                {
                    case "danhmucmt":
                        plqlmtloadcontrol.Controls.Add(LoadControl("QuanLyDanhMucTV/danhmucthanhvien.ascx"));
                        break;
                    case "danhmucncc":
                        plqlmtloadcontrol.Controls.Add(LoadControl("QuanLyNhaCungCap/danhmucnhacungcaploadcontrol.ascx"));
                        break;
                        
                }
            }
            if(Request.QueryString["thaotac"] != null)
            {
                thaotac = Request.QueryString["thaotac"];
                switch (thaotac)
                {
                    case "themmoimt":
                        plqlmtloadcontrol.Controls.Add(LoadControl("QuanLyDanhMucTV/themmoithanhvien.ascx"));
                        break;
                    case "chinhsua":
                        plqlmtloadcontrol.Controls.Add(LoadControl("QuanLyDanhMucTV/themmoithanhvien.ascx"));
                        break;
                }
            }
        }
    }
}