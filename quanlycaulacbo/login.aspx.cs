﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace quanlyphongthuchanh
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtDangNhap_Click(object sender, EventArgs e)
        {
            DataTable dt = quanlycaulacbo.loginadmin.admin(tbTenDangNhap.Text, tbmatkhau.Text);
            if(dt.Rows.Count > 0)
            {
                Session["login"] = "1";
                Session["username"] = dt.Rows[0]["username"];
                Response.Redirect("/AdminDhv.aspx?modul=qlmt&modulphu=danhmucmt");
            }
            else
            {
                ltrThongBao.Text = "<div style='color:red; padding: 45px 5px; text-align: center'>Tên đăng nhập hoặc mật khẩu không chính xác! </div>";
            }
        }
    }
}